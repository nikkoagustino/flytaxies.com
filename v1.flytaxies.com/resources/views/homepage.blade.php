@extends('template')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 home-desc-divider" style="padding: 0">
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			  <!-- Indicators -->
			  <ol class="carousel-indicators hidden-xs">
			    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
			  </ol>

			  <!-- Wrapper for slides -->
			  <div class="carousel-inner" role="listbox">
			    <div class="item active">
			      <img src="{{ url('assets/img/slide/1.webp') }}" alt="...">
			    </div>
			    <div class="item">
			      <img src="{{ url('assets/img/slide/5.webp') }}" alt="...">
			    </div>
			    <div class="item">
			      <img src="{{ url('assets/img/slide/3.webp') }}" alt="...">
			    </div>
			    <div class="item">
			      <img src="{{ url('assets/img/slide/4.webp') }}" alt="...">
			    </div>
			  </div>

			  <!-- Controls -->
			  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a>
			</div>
		</div>
	</div>
</div>
<div class="container home-desc slideshow-separation">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<center><h1 style="color: black; font-weight: bold">THE MOMENT</h1></center>
			<center><span style="font-size: 16px">
			Di masa <b>PANDEMIK</b> ini merupakan waktu yang tepat untuk merasakan pengalaman terbang dengan <b>PESAWAT PRIBADI</b>.<br>Karena kurangnya informasi mengenai apa keuntungan penerbangan dengan pesawat pribadi, maka,<br>Kami <b>FlyTaxies</b> akan memberikan segala akses dan <b>informasi</b> bagi kalian para traveller.
<br><br>
Di luar dari kelas dan style, private jet akan membawa <b>berbagai manfaat</b> bagi para penggunanya.
<br><br>
Di bawah ini dapat di lihat berbagai <b>kekhawatiran dan masalah</b> yang dapat di jumpai sebagai traveller.<br><br></span>
<img src="{{ url('assets/img/frontpage/experience.webp') }}" alt="">
<img src="{{ url('assets/img/frontpage/hospitality.webp') }}" alt="">
<img src="{{ url('assets/img/frontpage/safety.webp') }}" alt="">
</center>
			<center><h1 style="color: black; font-weight: bold">PROBLEMS</h1></center>
		</div>
		</div>
				<div class="row">
			<div class="hidden-lg hidden-md hidden-sm col-xs-12">
				<img src="{{ url('assets/img/frontpage/masalah/1.webp') }}" alt="">
			</div>
			<div class="hidden-lg hidden-md hidden-sm col-xs-12">
				<span>
					Khawatir terbang dengan <b><u>orang yang tidak di kenal</u></b> di masa pandemik ini
					<br><br>
				</span>
			</div>

			<div class="hidden-lg hidden-md hidden-sm col-xs-12">
				<img src="{{ url('assets/img/frontpage/masalah/2.webp') }}" alt="">
			</div>
			<div class="hidden-lg hidden-md hidden-sm col-xs-12">
				<span>
					Secara statistik besar kemungkinan beberapa dari orang yg berjalan2 atau duduk di samping anda bisa saja <b><u>sedang
					terinfeksi</u></b> virus tanpa gejala atau baru saja sembuh yang masih membawa
					<br><br>
				</span>
			</div>
		</div>
		
		<div class="row">
			<div class="hidden-lg hidden-md hidden-sm col-xs-12">
				<img src="{{ url('assets/img/frontpage/masalah/3.webp') }}" alt="">
			</div>
			<div class="hidden-lg hidden-md hidden-sm col-xs-12">
				<span>
					Lelah <b><u>mengantri</u></b> untuk <b><u>saat</u></b> akan <b><u>checkin</u></b>, menunggu pesawat dan masuk ke dalam pesawat
					<br><br>
				</span>
			</div>

			<div class="hidden-lg hidden-md hidden-sm col-xs-12">
				<img src="{{ url('assets/img/frontpage/masalah/4.webp') }}" alt="">
			</div>
			<div class="hidden-lg hidden-md hidden-sm col-xs-12">
				<span>
					Lelah <b><u>menunggu keterlambatan</u></b> pesawat sebelumnya
					<br><br>
				</span>
			</div>
		</div>

		<div class="row">
			<div class="hidden-lg hidden-md hidden-sm col-xs-12">
				<img src="{{ url('assets/img/frontpage/masalah/5.webp') }}" alt="">
			</div>
			<div class="hidden-lg hidden-md hidden-sm col-xs-12">
				<span>
					<b><u>Tidak pernah tahu</u></b> bagaimana <b><u>caranya</u></b> bisa terbang secara pribadi
					<br><br>
				</span>
			</div>

			<div class="hidden-lg hidden-md hidden-sm col-xs-12">
				<img src="{{ url('assets/img/frontpage/masalah/6.webp') }}" alt="">
			</div>
			<div class="hidden-lg hidden-md hidden-sm col-xs-12">
				<span>
					<b><u>Tidak pernah tahu</u></b> bahwa <b><u>harga sewa</u></b> pesawat ternyata tidak semahal itu
					<br><br>
				</span>
			</div>
		</div>

		<div class="row">
			<div class="hidden-lg hidden-md hidden-sm col-xs-12">
				<img src="{{ url('assets/img/frontpage/masalah/7.webp') }}" alt="">
			</div>
			<div class="hidden-lg hidden-md hidden-sm col-xs-12">
				<span>
					<b><u>Repot Rapid Test / Mencari surat</u></b> keterangan sehat untuk penerbangan domestik?
					<br><br>
				</span>
			</div>

			<div class="hidden-lg hidden-md hidden-sm col-xs-12">
				<img src="{{ url('assets/img/frontpage/masalah/8.webp') }}" alt="">
			</div>
			<div class="hidden-lg hidden-md hidden-sm col-xs-12">
				<span>
					<b><u>Kesulitan</u></b> dalam hal <b><u>mencari</u></b> penerbangan untuk <b><u>MEDIVAC</u></b> / Medical Evacuation (Air Ambulance)
					<br><br>
				</span>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
				<img src="{{ url('assets/img/frontpage/masalah/1.webp') }}" alt="">
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs masalah-left">
				<span>
					Khawatir terbang dengan <b><u>orang yang tidak di kenal</u></b> di masa pandemik ini
				</span>
			</div>

			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
				<img src="{{ url('assets/img/frontpage/masalah/2.webp') }}" alt="">
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs masalah-left">
				<span>
					Secara statistik besar kemungkinan beberapa dari orang yg berjalan2 atau duduk di samping anda bisa saja <b><u>sedang
					terinfeksi</u></b> virus tanpa gejala atau baru saja sembuh yang masih membawa
				</span>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs masalah-right">
				<span>
					Lelah <b><u>mengantri</u></b> untuk <b><u>saat</u></b> akan <b><u>checkin</u></b>, menunggu pesawat dan masuk ke dalam pesawat
				</span>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
				<img src="{{ url('assets/img/frontpage/masalah/3.webp') }}" alt="">
			</div>

			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs masalah-right">
				<span>
					Lelah <b><u>menunggu keterlambatan</u></b> pesawat sebelumnya
				</span>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
				<img src="{{ url('assets/img/frontpage/masalah/4.webp') }}" alt="">
			</div>
		</div>

		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
				<img src="{{ url('assets/img/frontpage/masalah/5.webp') }}" alt="">
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs masalah-left">
				<span>
					<b><u>Tidak pernah tahu</u></b> bagaimana <b><u>caranya</u></b> bisa terbang secara pribadi
				</span>
			</div>

			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
				<img src="{{ url('assets/img/frontpage/masalah/6.webp') }}" alt="">
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs masalah-left">
				<span>
					<b><u>Tidak pernah tahu</u></b> bahwa <b><u>harga sewa</u></b> pesawat ternyata tidak semahal itu
				</span>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs masalah-right">
				<span>
					<b><u>Repot Rapid Test / Mencari surat</u></b> keterangan sehat untuk penerbangan domestik?
				</span>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
				<img src="{{ url('assets/img/frontpage/masalah/7.webp') }}" alt="">
			</div>

			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs masalah-right">
				<span>
					<b><u>Kesulitan</u></b> dalam hal <b><u>mencari</u></b> penerbangan untuk <b><u>MEDIVAC</u></b> / Medical Evacuation (Air Ambulance)
				</span>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
				<img src="{{ url('assets/img/frontpage/masalah/8.webp') }}" alt="">
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<center><h1 style="color: black; font-weight: bold">SOLUSI</h1></center>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<img src="{{ url('assets/img/frontpage/solusi/1.webp') }}" alt="">
				Terbang bersama dengan saudara / <b><u>orang yang anda kenal</u></b> dan yakin tidak sakit<br><br>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<img src="{{ url('assets/img/frontpage/solusi/2.webp') }}" alt="">
				Tidak perlu repot dengan <b><u>antrian checkin / masuk pesawat</u></b> karena semuanya sudah di service<br><br>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<img src="{{ url('assets/img/frontpage/solusi/3.webp') }}" alt="">
				<b><u>Schedule sangat Fleksible</u></b>, mengikuti kapanpun pencharter mau (min 3 jam sebelum ETD)<br><br>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<img src="{{ url('assets/img/frontpage/solusi/4.webp') }}" alt="">
				FlyTaxies memberikan solusi kepada banyak orang yang <b><u>BELUM PERNAH TAU</u></b> mengenai charter Pesawat<br><br>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<img src="{{ url('assets/img/frontpage/solusi/5.webp') }}" alt="">
				Menunjukan kepada dunia bahwa Mencharter Pesawat pribadi <b><u>bisa dilakukan dengan sangat mudah</u></b> dengan menghubungi kami<br><br>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<img src="{{ url('assets/img/frontpage/solusi/6.webp') }}" alt="">
				Rapid Test / Surat keterangan sehat <b><u>dapat kami persiapkan bagi anda</u></b> untuk penerbangan jet pribadi<br><br>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12">
				<img src="{{ url('assets/img/frontpage/solusi/7.webp') }}" alt="">
				<b><u>MEDIVAC</u></b> di genggaman anda, kapanpun dan kemanapun kami selalu SIAP melayani anda<br><br>
			</div>
		</div>
</div>
<div class="container-fluid contact-wrapper">
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 maps">
			<div>
			<span class="glyphicon glyphicon-map-marker"></span>
			<br>
			Ruko Elang Laut Boulevard Blok B No. 5<br>
			Pantai Indah Kapuk, Penjaringan<br>
			Jakarta, Indonesia
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 nomaps">
			<div>
				<h1>CONTACT US</h1>
				If you have any inquiries and/or business offer, feel free to contact us via:
				<br><br><br>
				<table>
					<tr>
						<td><span class="fa fa-phone"></span></td>
						<td><span class="font-xs">Phone</span><br><a href="tel:+6281390001195">+62813-9000-1195</a><br>
							<i style="font-size: 12px">(24 hours)</i><br><a href="tel:+622122662985">(021) 2266-2985</a><br>
							<i style="font-size: 12px">(23:00 - 09:00 over hour)</i><br></td>
					</tr>
					<tr>
						<td><br><span class="fa fa-envelope"></span></td>
						<td><br>Email<br><a href="mailto:info@flytaxies.com">info@flytaxies.com</a></td>
					</tr>
				</table>
				<br><br><br>
			</div>
		</div>
	</div>
</div>
@endsection