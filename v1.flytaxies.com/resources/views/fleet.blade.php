@extends('template')
@section('content')
<div class="container home-desc">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 home-desc-divider"></div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<img src="{{ url('assets/img/be40/big.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/be40/1.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/be40/2.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/be40/3.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/be40/4.jpg') }}" alt="">
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h1><b>Hawker 400</b></h1>
			<h4><b>Specification</b></h4>
			<table class="table">
				<tr>
					<td>Crew</td>
					<td>2 pilots</td>
				</tr>
				<tr>
					<td>Capacity</td>
					<td>6 persons</td>
				</tr>
				<tr>
					<td>Payload</td>
					<td>2,015 lb / 913 kg</td>
				</tr>
				<tr>
					<td>Powerplant</td>
					<td>2x <b>Pratt & Whitney JT15D-5R</b></td>
				</tr>
				<tr>
					<td>Range</td>
					<td>1,180 nm / 2,185 km</td>
				</tr>
				<tr>
					<td>Cruise Speed</td>
					<td>443 knots / 820 km/h</td>
				</tr>
			</table>
			<br><br>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h1><b>Hawker 900XP</b></h1>
			<h4><b>Specification</b></h4>
			<table class="table">
				<tr>
					<td>Crew</td>
					<td>2 pilots</td>
				</tr>
				<tr>
					<td>Capacity</td>
					<td>8 persons</td>
				</tr>
				<tr>
					<td>Payload</td>
					<td>1,950 lb / 884 kg</td>
				</tr>
				<tr>
					<td>Powerplant</td>
					<td>2x <b>Honeywell TFE731-50BR</b></td>
				</tr>
				<tr>
					<td>Range</td>
					<td>2,733 nm / 5,061 km</td>
				</tr>
				<tr>
					<td>Cruise Speed</td>
					<td>446 knots / 826 km/h</td>
				</tr>
			</table>
			<br><br>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<img src="{{ url('assets/img/h25b/big.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/h25b/1.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/h25b/2.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/h25b/3.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/h25b/4.jpg') }}" alt="">
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<img src="{{ url('assets/img/a320/big.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/a320/1.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/a320/2.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/a320/3.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/a320/4.jpg') }}" alt="">
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h1><b>Airbus A320</b></h1>
			<h4><b>Specification</b></h4>
			<table class="table">
				<tr>
					<td>Crew</td>
					<td>2 pilots</td>
				</tr>
				<tr>
					<td>Capacity</td>
					<td>160 passengers</td>
				</tr>
				<tr>
					<td>Payload</td>
					<td>44,000 lb</td>
				</tr>
				<tr>
					<td>Powerplant</td>
					<td>2x <b>CFM International CFM56-5B</b></td>
				</tr>
				<tr>
					<td>Range</td>
					<td>3,300 nm / 6,112 km</td>
				</tr>
				<tr>
					<td>Cruise Speed</td>
					<td>470 knots / 871 km/h</td>
				</tr>
			</table>
			<br><br>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h1><b>Boeing 737-800</b></h1>
			<h4><b>Specification</b></h4>
			<table class="table">
				<tr>
					<td>Crew</td>
					<td>2 pilots</td>
				</tr>
				<tr>
					<td>Capacity</td>
					<td>189 passengers</td>
				</tr>
				<tr>
					<td>Payload</td>
					<td>45,200 lb</td>
				</tr>
				<tr>
					<td>Powerplant</td>
					<td>2x <b>CFM International CFM56-7B</b></td>
				</tr>
				<tr>
					<td>Range</td>
					<td>2,935 nm / 5,436 km</td>
				</tr>
				<tr>
					<td>Cruise Speed</td>
					<td>455 knots / 842 km/h</td>
				</tr>
			</table>
			<br><br>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<img src="{{ url('assets/img/b738/big.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/b738/1.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/b738/2.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/b738/3.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/b738/4.jpg') }}" alt="">
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<img src="{{ url('assets/img/b739/big.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/b739/1.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/b739/2.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/b739/3.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/b739/4.jpg') }}" alt="">
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h1><b>Boeing 737-900ER</b></h1>
			<h4><b>Specification</b></h4>
			<table class="table">
				<tr>
					<td>Crew</td>
					<td>2 pilots</td>
				</tr>
				<tr>
					<td>Capacity</td>
					<td>214 passengers</td>
				</tr>
				<tr>
					<td>Payload</td>
					<td>44,600 lb</td>
				</tr>
				<tr>
					<td>Powerplant</td>
					<td>2x <b>CFM International CFM56-7B</b></td>
				</tr>
				<tr>
					<td>Range</td>
					<td>2,950 nm / 5,460 km</td>
				</tr>
				<tr>
					<td>Cruise Speed</td>
					<td>455 knots / 842 km/h</td>
				</tr>
			</table>
			<br><br>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h1><b>ATR72-500</b></h1>
			<h4><b>Specification</b></h4>
			<table class="table">
				<tr>
					<td>Crew</td>
					<td>2 pilots</td>
				</tr>
				<tr>
					<td>Capacity</td>
					<td>68 passengers</td>
				</tr>
				<tr>
					<td>Payload</td>
					<td>15,873 lb</td>
				</tr>
				<tr>
					<td>Powerplant</td>
					<td>2x <b>Pratt & Whitney PW127F</b></td>
				</tr>
				<tr>
					<td>Range</td>
					<td>785 nm / 1,453 km</td>
				</tr>
				<tr>
					<td>Cruise Speed</td>
					<td>275 knots / 510 km/h</td>
				</tr>
			</table>
			<br><br>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<img src="{{ url('assets/img/at75/big.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/at75/1.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/at75/2.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/at75/3.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/at75/4.jpg') }}" alt="">
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<img src="{{ url('assets/img/at76/big.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/at76/1.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/at76/2.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/at76/3.jpg') }}" alt="">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<img src="{{ url('assets/img/at76/4.jpg') }}" alt="">
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h1><b>ATR72-600</b></h1>
			<h4><b>Specification</b></h4>
			<table class="table">
				<tr>
					<td>Crew</td>
					<td>2 pilots</td>
				</tr>
				<tr>
					<td>Capacity</td>
					<td>70 passengers</td>
				</tr>
				<tr>
					<td>Payload</td>
					<td>16,534 lb</td>
				</tr>
				<tr>
					<td>Powerplant</td>
					<td>2x <b>Pratt & Whitney PW127M</b></td>
				</tr>
				<tr>
					<td>Range</td>
					<td>825 nm / 1,527 km</td>
				</tr>
				<tr>
					<td>Cruise Speed</td>
					<td>275 knots / 510 km/h</td>
				</tr>
			</table>
			<br><br>
		</div>
	</div>
</div>
@endsection