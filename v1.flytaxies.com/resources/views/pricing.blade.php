@extends('template')
@section('content')

<div class="container home-desc">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 home-desc-divider"></div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h1 style="color: black; font-weight: bold; text-align: center">PRICING INFORMATION</h1>
Kebanyakan orang tidak mengetahui bagaimana cara terbang dengan Private jet dan juga <b><u>Berapa harga kisaran</u></b> untuk terbang dengan pesawat private jet.
<br><br>
Kami <b><u>berkomitmen</u></b> ingin membuka pintu penerbangan private jet / charter ini <b><u>bagi siapapun</u></b> dengan cara memberikan cara termudah bagi siapapun yang ingin menggunakan private jet kami atau pesawat besar lainnya.
<br><br>
Begitu juga dengan rekan-rekan di luar sana banyak pula yang <b><u>tidak memahami</u></b> bahwa mencharter pesawat besar tidak lah se mahal yang di bayangkan.
<br><br>
Maka dari itu kami akan berikan <b>CONTOH</b> beberapa harga di bawah ini untuk memberikan gambaran bagi kalian:
<br><br>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
	<div class="panel panel-default">
  <div class="panel-heading">
HALIM-DENPASAR
</div>
  <div class="panel-body">
Menggunakan <b>Hawker 400</b> : Rp. 268jt per pesawat
<br>Dengan kapasitas 6 orang, jadi Cost per kursi <b><u><i>44,6jt PP</i></u></b>.
<br>Rapid test datang ke tempat client, kemudahan checkin dan Silver Bird ke bandara dari jabodetabek ke Halim Airport. Dimana pesawat itu akan terbang bolak balik artinya <u><i>include kursi untuk 6 orang</i></u> HLP-DPS, transit 1 jam dan kursi 6 orang pula untuk DPS-HLP. Biaya inap harus di perhitungkan kembali.
  </div>
</div>

	<div class="panel panel-default">
  <div class="panel-heading">
KUALALUMPUR - JAKARTA
</div>
  <div class="panel-body">
Menggunakan <b>Boeing 737-900ER</b> : Total Biaya 522jt Rupiah
<br>Total Max PAX 215 org. Cost per Kursi : <b><u><i>2.4jt</i></u></b> Rupiah (One way saja)
  </div>
</div>
</div>

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
	<div class="panel panel-default">
  <div class="panel-heading">
HALIM-DENPASAR
</div>
  <div class="panel-body">
Menggunakan <b>Hawker 850-900</b> : Rp. 380jt per pesawat
<br>Dengan kapasitas 8 orang, jadi Cost per kursi <b><u><i>47.5jt PP</i></u></b>.
<br>Rapid test datang ke tempat client, kemudahan checkin dan Silver Bird ke bandara dari jabodetabek ke Halim Airport. Dimana pesawat itu akan terbang bolak balik artinya <u><i>include kursi untuk 8 orang</i></u> HLP-DPS, transit 1 jam dan kursi 8 orang pula untuk DPS-HLP. Biaya inap harus di perhitungkan kembali.
  </div>
</div>

	<div class="panel panel-default">
  <div class="panel-heading">
MANADO - LABUHA
</div>
  <div class="panel-body">
Menggunakan <b>ATR-72</b>
<br>Total Biaya 200jt Rupiah untuk 71 orang penumpang
<br>Jadi total biaya per seat : <b><u><i>2.8 Juta</i></u></b> per Seat (Termasuk penerbangan kembali setelah penerbangan tersebut)
  </div>
</div>
</div>
* <i>Harga di atas hanya contoh saja dapat berubah sewaktu-waktu, karena harga selalu berubah dengan berbagai faktor seperti Rate USD, keterbatasan pesawat dan lain-lain. Untuk harga terupdate silahkan hubungi kami dengan menklik tombol CONTACT US 24 jam service.</i>
<br><br>

Banyak sekali orang yang tidak ketahui bahwa charter flight tidak semahal yang di kira <b><u>jika di bayar bersama-sama</u></b> dengan kawan-kawan / orang terdekat kita bersama, Dengan Private / Charter flight anda bisa tentukan destinasi manapun yang anda sukai, dan memiliki <b><u>fleksibilitas dan menentukan schedule</u></b> terbang anda.<br><br><br><br>
		</div>
	</div>
</div>
@endsection