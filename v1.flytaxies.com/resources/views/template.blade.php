<!DOCTYPE html>
<html>
<head>
	<title>FLY TAXIES - Help Me Fly Anytime and Anywhere</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="robot" content="index, follow">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ url('assets/style.css') }}">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
</head>
<body>
<div class="container-fluid navbar-row navbar-fixed-top">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 navbar-contact">
			<div class="container">
				<span class="fa fa-phone"></span> &nbsp; +62813-9000-1195 <i>(24 hours)</i> / (021) 2266-2985 <i>(23:00-09:00)</i> &nbsp; &nbsp; <span class="fa fa-envelope"></span> &nbsp; info@flytaxies.com
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-9">
					<img src="{{ url('assets/img/flytaxies.webp') }}" class="img-nav-color" alt="">
				</div>
				<div class="col-xs-3 hidden-sm hidden-md hidden-lg">
					<span class="glyphicon glyphicon-menu-hamburger pull-right mobileNavToggle" onclick="$('.mobileNavWrapper').toggle(200);"></span>
				</div>

				<!-- =================================================================================== -->
				<div class="col-xs-12 hidden-sm hidden-md hidden-lg mobileNavWrapper" hidden>
					<ul class="nav navbar-nav navbar-right">
						<li><a <?= (Request::is('/')) ? 'class="active"' : ''; ?> href="{{ url('/') }}">HOME</a></li>
						<li><a <?= (Request::is('pricing')) ? 'class="active"' : ''; ?> href="{{ url('pricing') }}">PRICING</a></li>
						<li><a <?= (Request::is('services')) ? 'class="active"' : ''; ?> href="{{ url('services') }}">SERVICES</a></li>
						<li><a <?= (Request::is('fleet')) ? 'class="active"' : ''; ?> href="{{ url('fleet') }}">FLEET</a></li>
						<li><a <?= (Request::is('contact-us')) ? 'class="active"' : ''; ?> href="{{ url('contact-us') }}">CONTACT US</a></li>
					</ul>
				</div>
				<!-- =================================================================================== -->


				<div class="col-lg-8 col-md-8 col-sm-8 hidden-xs">
					<nav class="navbar navbar-default">
						<div class="container-fluid">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>

							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul class="nav navbar-nav navbar-right">
									<li><a <?= (Request::is('/')) ? 'class="active"' : ''; ?> href="{{ url('/') }}">HOME</a></li>
									<li><a <?= (Request::is('pricing')) ? 'class="active"' : ''; ?> href="{{ url('pricing') }}">PRICING</a></li>
									<li><a <?= (Request::is('services')) ? 'class="active"' : ''; ?> href="{{ url('services') }}">SERVICES</a></li>
									<li><a <?= (Request::is('fleet')) ? 'class="active"' : ''; ?> href="{{ url('fleet') }}">FLEET</a></li>
									<li><a <?= (Request::is('contact-us')) ? 'class="active"' : ''; ?> href="{{ url('contact-us') }}">CONTACT US</a></li>
								</ul>
							</div><!-- /.navbar-collapse -->
						</div><!-- /.container-fluid -->
					</nav>
				</div>
			</div>
		</div>
	</div>
</div>

@yield('content')

<div class="container-fluid navbar-bottom">
	<div class="row">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<img src="{{ url('assets/img/flytaxies.webp') }}" class="img-nav-white" alt="">
				<div class="nav-bottom-separator"></div>
				<ul class="nav navbar-nav hidden-xs">

					<li><a href="{{ url('/') }}">HOME</a></li>
					<li><a href="{{ url('pricing') }}">PRICING</a></li>
					<li><a href="{{ url('services') }}">SERVICES</a></li>
					<li><a href="{{ url('fleet') }}">FLEET</a></li>
					<li><a href="{{ url('contact-us') }}">CONTACT US</a></li>
				</ul>
				<div class="pull-right">
					<div class="nav-bottom-separator"></div>
					&nbsp;&nbsp;
					<span class="fa fa-facebook"></span>
					<span class="fa fa-twitter"></span>
					<span class="fa fa-youtube"></span>
					<span class="fa fa-instagram"></span>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>

<div class="container-fluid copyright">
	<div class="row">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				Copyright &copy; <?= (date('Y') == 2019) ? date('Y') : '2019 - '.date('Y') ?> FlyTaxies Indonesia &nbsp;
			</div>
		</div>
	</div>
	</div>
</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	<script src="{{ url('assets/script.js') }}"></script>
	<script type="text/javascript">
		$('.carousel').carousel({
		  interval: 2000
		});
	</script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-176809700-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-176809700-1');
</script>

</body>
</html>