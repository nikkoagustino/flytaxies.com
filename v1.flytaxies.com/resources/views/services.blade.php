@extends('template')
@section('content')
<div class="container home-desc">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 home-desc-divider"></div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<center><h1><b>PRIVATE VIP FLIGHT</b></h1>
			Di bawah ini Anda dapat melihat berbagai macam service yang dapat kami berikan bagi Anda yang ingin merasakan / memiliki kenangan <br>yang tidak akan pernah terlupakan bersama dengan orang terdekat Anda.<br><br>
			Kami menyediakan charter pesawat untuk keperluan pribadi Anda seperti:
		</center>
		</div>
	</div>

<div class="row">
		<div class="hidden-lg hidden-md hidden-sm col-xs-12">
			<img src="{{ url('assets/img/services/business.webp') }}">
		</div>
		<div class="hidden-lg hidden-md hidden-sm col-xs-12">
			<h1><b>BUSINESS</b></h1>
			Kami mengerti the value of time dan fleksibilitas bagi orang orang yang nilai waktunya tidak bisa digantikan dengan uang.
<br><br>
Kami percaya bahwa dengan adanya service kami dapat memberikan efisiensi waktu yang pada akhirnya akan membuka kesempatan yang lebih banyak bagi Anda / perusahaan Anda.
<br><br>
Itulah mengapa prioritas kami akan mengutamakan fleksibilitas mengikuti dengan schedule permintaan Anda.
			<br><br>
			<br><br>
		</div>
	</div>
	<div class="row">
		<div class="hidden-lg hidden-md hidden-sm col-xs-12">
			<img src="{{ url('assets/img/services/family.webp') }}">
		</div>
		<div class="hidden-lg hidden-md hidden-sm col-xs-12">
			<h1><b>FAMILY</b></h1>
Khawatir akan keselamatan keluarga atau orang terdekat Anda, kami mengutamakan kebersihan seluruh kabin di dalam pesawat kami.
<br><br>
Untuk menjaga keselamatan klien kami dari segala aspek, baik itu penerbangan dengan menggunakan pilot terbaik dan berpengalaman dan juga kebersihan interior pesawat kami.
			<br><br>
			<br><br>

		</div>
	</div>
	<div class="row">
		<div class="hidden-lg hidden-md hidden-sm col-xs-12">
			<img src="{{ url('assets/img/services/medivac.webp') }}">
		</div>
		<div class="hidden-lg hidden-md hidden-sm col-xs-12">
			<h1><b>MEDIVAC (EMERGENCY AIR AMBULANCE SERVICE)</b></h1>
			Kami melayani evakuasi medis melalui udara bagi yang membutuhkan perawatan medis khusus dan tidak dapat menggunakan pesawat komersial
<br><br>
Karena kami paham seberapa pentingnya untuk dapat membantu dalam menjaga kesehatan dimana terkadang memerlukan penerbangan untuk mengevakuasi dengan tujuan menyelamatkan nyawa.
<br><br>
Karena kami percaya dengan memberikan akses tenaga medis dari seluruh penjuru dunia dapat menaikkan kesempatan kehidupan bagi yang membutuhkan.
			<br><br>
			<br><br>
		</div>
	</div>
	<div class="row">
		<div class="hidden-lg hidden-md hidden-sm col-xs-12">
			<img src="{{ url('assets/img/services/prewed.webp') }}">
		</div>
		<div class="hidden-lg hidden-md hidden-sm col-xs-12">
			<h1><b>PRE WEDDING PHOTOGRAPH</b></h1>
			Buat momen yang tidak pernah terlupakan bersama orang yang Anda cintai.
<br><br>
Waktu tidak akan pernah dapat terulang kembali. Pengalaman juga belum tentu dapat terulang kembali.
<br><br>
Maka dari itu kami percaya dengan memberikan service foto pre wedding bagi Anda dan orang yang Anda cintai agar momen ini tidak akan terlupakan seumur hidup.
			<br><br>
			<br><br>
		</div>
	</div>
	<div class="row">
		<div class="hidden-lg hidden-md hidden-sm col-xs-12">
			<img src="{{ url('assets/img/services/honeymoon.webp') }}">
		</div>
		<div class="hidden-lg hidden-md hidden-sm col-xs-12">
			<h1><b>HONEYMOON</b></h1>
Pernikahan adalah momen yang tidak pernah dapat dilupakan. Hidup ini bukan hanya memprioritaskan materi.
<br><br>
Setiap momen di dalam hidup akan menjadi sebuah kenangan yang telah terlewatkan.
<br><br>
Pernikahan hanya dilakukan dalam suatu momen dalam hidup, maka dari itu pastikan Anda memiliki kenangan yang sepadan.
			<br><br>
			<br><br>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 hidden-xs">
			<br><br><br>
			<h1><b>BUSINESS</b></h1>
			Kami mengerti the value of time dan fleksibilitas bagi orang orang yang nilai waktunya tidak bisa digantikan dengan uang.
<br><br>
Kami percaya bahwa dengan adanya service kami dapat memberikan efisiensi waktu yang pada akhirnya akan membuka kesempatan yang lebih banyak bagi Anda / perusahaan Anda.
<br><br>
Itulah mengapa prioritas kami akan mengutamakan fleksibilitas mengikuti dengan schedule permintaan Anda.
			<br><br>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 hidden-xs">
			<img src="{{ url('assets/img/services/business.webp') }}">
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 hidden-xs">
			<img src="{{ url('assets/img/services/family.webp') }}">
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 hidden-xs">
			<br><br><br><br>
			<h1><b>FAMILY</b></h1>
Khawatir akan keselamatan keluarga atau orang terdekat Anda, kami mengutamakan kebersihan seluruh kabin di dalam pesawat kami.
<br><br>
Untuk menjaga keselamatan klien kami dari segala aspek, baik itu penerbangan dengan menggunakan pilot terbaik dan berpengalaman dan juga kebersihan interior pesawat kami.
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 hidden-xs">
			<br><br><br><br>
			<h1><b>MEDIVAC (EMERGENCY AIR AMBULANCE SERVICE)</b></h1>
			Kami melayani evakuasi medis melalui udara bagi yang membutuhkan perawatan medis khusus dan tidak dapat menggunakan pesawat komersial
<br><br>
Karena kami paham seberapa pentingnya untuk dapat membantu dalam menjaga kesehatan dimana terkadang memerlukan penerbangan untuk mengevakuasi dengan tujuan menyelamatkan nyawa.
<br><br>
Karena kami percaya dengan memberikan akses tenaga medis dari seluruh penjuru dunia dapat menaikkan kesempatan kehidupan bagi yang membutuhkan.
			<br><br>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 hidden-xs">
			<img src="{{ url('assets/img/services/medivac.webp') }}">
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 hidden-xs">
			<img src="{{ url('assets/img/services/prewed.webp') }}">
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 hidden-xs">
			<br><br><br><br>
			<h1><b>PRE WEDDING PHOTOGRAPH</b></h1>
			Buat momen yang tidak pernah terlupakan bersama orang yang Anda cintai.
<br><br>
Waktu tidak akan pernah dapat terulang kembali. Pengalaman juga belum tentu dapat terulang kembali.
<br><br>
Maka dari itu kami percaya dengan memberikan service foto pre wedding bagi Anda dan orang yang Anda cintai agar momen ini tidak akan terlupakan seumur hidup.
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 hidden-xs">
			<br><br><br><br>
			<h1><b>HONEYMOON</b></h1>
Pernikahan adalah momen yang tidak pernah dapat dilupakan. Hidup ini bukan hanya memprioritaskan materi.
<br><br>
Setiap momen di dalam hidup akan menjadi sebuah kenangan yang telah terlewatkan.
<br><br>
Pernikahan hanya dilakukan dalam suatu momen dalam hidup, maka dari itu pastikan Anda memiliki kenangan yang sepadan.
			<br><br>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 hidden-xs">
			<img src="{{ url('assets/img/services/honeymoon.webp') }}">
		</div>
	</div>
</div>
@endsection