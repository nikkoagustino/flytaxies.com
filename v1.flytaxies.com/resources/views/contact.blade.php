@extends('template')
@section('content')
<div class="container home-desc">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 home-desc-divider"></div>
	</div>
	<div class="row">
		<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
			<h2>Contact Form</h2>
			<hr>
			<form method="post">
				<input class="form-control" type="text" placeholder="Nama" required="required"></input>
				<input class="form-control" type="email" placeholder="Email" required="required"></input>
				<select name="message_subject" required="required" class="form-control">
					<option disabled="disabled" selected="selected">-- Pilih Subyek</option>
					<option value="Pemesanan Charter">Pemesanan Charter</option>
					<option value="Pertanyaan Umum">Pertanyaan Umum</option>
					<option value="Kritik dan Komplain">Kritik dan Komplain</option>
					<option value="Lainnya">Lainnya</option>
				</select>
				<textarea name="message_text" rows="10" class="form-control"></textarea>
				<input type="submit" class="btn btn-transparent">
			</form>
		</div>
		<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12" style="font-size: 16px">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<img src="{{ url('assets/img/maps.png') }}">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">
					<table>
						<tr>
							<td><span class="fa fa-phone fav-contact"></span></td>
							<td>Phone<br><a href="tel:+6281390001195">+62813-9000-1195</a> <br><b style="font-size: 12px"><u>(24 hours)</u></b><br><a href="tel:+622122662985">(021) 2266-2985</a> <br><b style="font-size: 12px"><u>(23:00 - 09:00 over hour)</u></b><br></td>
						</tr>
						<tr>
							<td><br><span class="fa fa-envelope fav-contact"></span></td>
							<td><br>Email<br><a href="mailto:info@flytaxies.com">info@flytaxies.com</a></td>
						</tr>
					</table>
				</div>
				<div class="col-lg-7 col-md-6 col-sm-6 col-xs-12">
					<table>
						<tr>
							<td><span class="glyphicon glyphicon-map-marker fav-contact"></span></td>
							<td>Address<br><b style="color: darkblue">
							Ruko Elang Laut Boulevard Blok B No. 5<br>
							Pantai Indah Kapuk, Penjaringan<br>
							Jakarta, Indonesia</b>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><br>
		<br>
		<br></div>
	</div>
</div>
@endsection