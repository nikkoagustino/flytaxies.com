<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('homepage');
});

Route::get('services', function() {
	return view('services');
});

Route::get('fleet', function() {
	return view('fleet');
});

Route::get('contact-us', function() {
	return view('contact');
});

Route::get('pricing', function() {
	return view('pricing');
});
